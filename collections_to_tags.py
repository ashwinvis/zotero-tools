from pprint import pprint

from base import MyZotero


zot = MyZotero()


def make_hashtag(col_name):
    """Transform a `Collection name` into a `#collection-name`."""
    hashtag = '#' + col_name.lower().replace(' ', '-')
    return hashtag


def current_tags(item):
    return set(d.get('tag') for d in item.get('data').get('tags'))


def tag_items(items, *col_names):
    """Given a list of items tag according to the prescribed collection names"""
    hashtags = [make_hashtag(cn) for cn in col_names]

    print("Tagging", hashtags, "on", len(items), "items")

    for item in items:
        if not set(hashtags).issubset(current_tags(item)):
            zot.add_tags(item, *hashtags)


def tag_items_in_collections(col_id, *col_names):
    """Discover all items, one-level down in a collection and tag them."""
    # `collection_items` does not recurse, i.e. collect items from subcollections
    items = zot.everything(zot.collection_items(col_id))
    tag_items(items, *col_names)



def tag_items_in_subcollections(col_id, *col_names):
    """Tag the items under subcollections with the parent collection name and
    the subcollection name, and recurse top to down."""
    subcols = zot.everything(zot.all_collections(col_id))
    print(len(subcols), "subcollections")
    for c in subcols:
        subcol_name = c['data']['name']
        subcol_id = c['key']

        # Recurse
        if col_id != subcol_id:
            names = *col_names, subcol_name
            tag_items_in_collections(subcol_id, *names)
            tag_items_in_subcollections(subcol_id, *names)


# NOTE: this would only tag items within the same collection
collections = zot.everything(zot.collections())
print(len(collections), "collections")
for c in collections:
    tag_items_in_collections(c['key'], c['data']['name'])


# NOTE: this would recurse and add multiple tags
collections_top = zot.everything(zot.collections_top())
print(len(collections_top), "top-level collections")
for c in collections_top:
    tag_items_in_subcollections(c['key'], c['data']['name'])

Getting started
---------------

Create a file `_creds.py` containing your [Zotero][Zotero] and [GitHub][GitHub] credentials. Modify
the values of the following variables:

```python
USERID = "my_zotero_id"
KEY = "super_secret_zotero_key"
GITHUB_ACCESS_TOKEN = "supersecret_github_personal_access_token_read_only"
```
[Zotero]: https://www.zotero.org/settings/key
[GitHub]: https://github.com/settings/tokens 

## Scripts

- `fix_caps.py`: Provides `fix_caps` function which ensures that titles are capitalized
- `fix_github.py`: Filter, transform and delete to fix metadata in a specific
collection.
- `fix_thesis.py`:  Filter a specific collection
- `unstar_github.py`: Unstar all repos listed on your GitHub profile
- `zoteroute.py`: Make a Zotero Quote
- `collections_to_tags.py`: Add #hashtags according to collection hierarchy

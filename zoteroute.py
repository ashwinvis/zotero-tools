"""zoteroute: A Zotero Quote a day"""
import pickle
import random
from pathlib import Path

from rich import markup
from rich.console import Console
from toolz import compose

from base import MyZotero

console = Console(width=100)
zot = MyZotero()


def cached_load(key, fallback_callback):
    """Try to open a pickled cache based on a key, if not execute the callback."""
    cache = Path.home() / ".cache" / "zotero-tools" / f"{key}.pkl"

    if cache.exists():  # TODO: and not too old
        with cache.open("rb") as pkl:
            obj = pickle.load(pkl)
    else:
        obj = fallback_callback(key)
        cache.parent.mkdir(exist_ok=True)
        try:
            with cache.open("wb") as pkl:
                pickle.dump(obj, pkl)
        except TypeError:
            print(f"Error while caching Zotero collection: {key}. Cleaning up.")
            cache.unlink()

    return obj


def quote(item):
    """Quote an item in summary."""
    # NOTE: uncomment to debug item data
    # console.print(item)
    # Shortcut to get an value from the dictionary
    get = item["data"].get

    if isinstance(abstract := get("abstractNote"), str):
        max_chars = 500
        abstract = markup.render(abstract.replace("\n", " ")[:max_chars])
    else:
        abstract = ""

    title = "[bold]{}[/bold]".format(get("shortTitle") or get("title"))

    console.print(title, abstract, sep=": ")

    console.print(
        "Link     :",
        get("DOI") or get("url") or get("date") or "",
        get("journalAbbreviation") or get("publicationTitle") or get("itemType"),
    )

    console.print("Zotero   :", item["links"]["alternate"]["href"])

    if (tags := get("tags")) :
        if isinstance(tags, list):
            keywords = ", ".join(i["tag"] for i in tags)
            console.print("Keywords :", keywords)


def rand_item(collection):
    """Fetch an item from a collection in random."""
    key = collection["key"]

    items = cached_load(key, lambda k: zot.everything(zot.collection_items(k)))
    while (item := random.choice(items)) :
        if "up" not in item["links"]:
            break

    return item


def rand_collection(collection_names=()):
    """Fetch a collection in random."""
    if not collection_names:
        raise NotImplementedError
    else:
        name = random.choice(collection_names)

    collection = cached_load(name, lambda n: zot.get_collection(n))
    return collection


zoteroute = compose(quote, rand_item, rand_collection)


if __name__ == "__main__":
    zoteroute(["github-stars-ashwinvis", "ABL", "Books and Courses"])
